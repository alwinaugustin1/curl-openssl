# cURL OpenSSL RPMsThe version of cURL shipped with CentOS 6 is built against the Mozilla Network Security Services (NSS) libraries. This package is a port built against OpenSSL. 


cURL with NSS defaults to TLS 1.0 and sometimes has issues connecting to servers with TLS 1.1 and TLS 1.2.

This package solves problems with errors like the following:

```* NSS error -12286* Error in TLS handshake, trying SSLv3...* SSL write: error -12286* Failed sending HTTP request* Connection #0 to host foo.bar.com left intactcurl: (55) SSL write: error -12286* Closing connection #0``````* SSLv3, TLS handshake, Client hello (1):* error:140770FC:SSL routines:SSL23_GET_SERVER_HELLO:unknown protocol* Closing connection 0curl: (35) error:140770FC:SSL routines:SSL23_GET_SERVER_HELLO:unknown protocol```# DownloadVisit [the release page](https://github.com/PerkDotCom/curl-openssl/releases) and download the packages for your system. If you're feeling a little hardcore you can build your own with the instructions below.# Build InstructionsHere are the directions for building your own copy based on this git repository.```
sudo yum groupinstall "Development Tools"
sudo yum install yum-utils
git clone https://github.com/PerkDotCom/curl-openssl.git ~/rpmbuild
wget -P ~/rpmbuild/SOURCES/ http://curl.haxx.se/download/trash/curl-7.19.7.tar.lzma
cd ~/rpmbuild/SPECS
sudo yum-builddep curl.spec
rpmbuild -ba curl.spec
sudo rpm -Uvh ~/rpmbuild/RPMS/x86_64/*.rpm
```